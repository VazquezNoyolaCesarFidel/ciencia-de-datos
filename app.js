var express = require('express');
var app = express();
app.set('view engine','ejs');

var PORT = process.env.PORT || 5000;
app.get("/",function(req,res){
  res.render("index");
});
app.listen(PORT,function(){
  console.log("Servidor corriendo en puerto 3000");
});
